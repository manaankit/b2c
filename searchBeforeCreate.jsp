<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
    <meta charset="utf-8">
    <title>
        Demo auto complete
    </title>
    <style>
* {
  box-sizing: border-box;
}
body {
  font: 16px Arial;  
}
.autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}
input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
  min-width:50px!important;
  max-width:99.99%!important;
  transition: width 0.25s;
  
}
input[type=text] {
  background-color: #f1f1f1;
  width: 60%;
  text-align:center;
}
input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
}
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}
.autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9; 
}
.autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
.button {
  display: inline-block;
  padding: 8px 18px;
  font-size: 18px;
  cursor: pointer;
  text-align: center;
  text-decoration: none;
  outline: none;
  color: #fff;
  background-color: #3399ff;
  border: none;
  border-radius: 15px;
  box-shadow: 0 9px #999;
}

.button:hover {background-color: #3e8e41}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}
</style>
	<script>
	window.addEventListener("load", function(){
	window.hinterXHR = new XMLHttpRequest();
	 window.hinterXHR.abort();
});

// Autocomplete for form
function hinter(event) {
    // retireve the input element
    var input = event.target;
	
    // retrieve the datalist element
    var huge_list = document.getElementById('huge_list');
	// minimum number of characters before we start to generate suggestions
    var min_characters = 0;

    if (input.value.length < min_characters ) { 
        return;
    } 
    else {     
        // abort any pending requests
        window.hinterXHR.abort();
        if(document.activeElement==cname)
        {
        window.hinterXHR.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                // We're expecting a json response so we convert it to an object
                var response = JSON.parse( this.responseText );
                // clear any previously loaded options in the datalist
                huge_list.innerHTML = "";
                response.forEach(function(item) {
                    // Create a new <option> element.
                    var option = document.createElement('option');
                    option.value = item;
                    // attach the option to the datalist element
                    huge_list.appendChild(option);
                });
            }
        };
		
        window.hinterXHR.open("GET", "http://localhost:8080/lennox-mdm/rest/service/v1/search/"+input.value+"/%20", true);
        }
        
        if(document.activeElement==cnumber)
        {
        window.hinterXHR.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                // We're expecting a json response so we convert it to an object
                var response = JSON.parse( this.responseText );
                // clear any previously loaded options in the datalist
                huge_list.innerHTML = "";
                response.forEach(function(item) {
                    // Create a new <option> element.
                    var option = document.createElement('option');
                    option.value = item;
                    // attach the option to the datalist element
                    huge_list.appendChild(option);
                  
                });
            }
        };
		
        window.hinterXHR.open("GET", "http://localhost:8080/lennox-mdm/rest/service/v1/search/%20/" + input.value, true);
        }
        window.hinterXHR.send();
    }
}
function updateField()
{
	window.hinterXHR.abort();
	var inputs = document.getElementsByTagName("input");
	var x = document.getElementsByTagName("input");
	var i;
	for (i = 0; i < x.length; i++) { 		
  		if(x[i].id=="cname" && x[i].value!="")
  		{
			window.hinterXHR.open("GET", "http://localhost:8080/lennox-mdm/rest/service/v1/searchResponse/"+x[i].value+"/%20", false);
 		}
 		else if(x[i].id=="cnumber" && x[i].value!="")
  		{
			window.hinterXHR.open("GET", "http://localhost:8080/lennox-mdm/rest/service/v1/searchResponse/%20/"+x[i].value, false);
 		}
 		
	}
    window.hinterXHR.setRequestHeader("Content-type", "application/json");
	window.hinterXHR.send("Your JSON Data Here");
	var jsonResponse = JSON.parse(window.hinterXHR.responseText);
	if(jsonResponse.CONTACT.CONTACT_SALU_CD==undefined)
	{
		document.getElementById("Salutation").value =" ";
	}
	else
	{
		document.getElementById("Salutation").value =jsonResponse.CONTACT.CONTACT_SALU_CD;
	}
	if(jsonResponse.CONTACT.CONTACT_FIRST_NM==undefined)
	{
		document.getElementById("first_name").value =" ";
	}
	else
	{
		document.getElementById("first_name").value =jsonResponse.CONTACT.CONTACT_FIRST_NM;
	}
	if(jsonResponse.CONTACT.CONTACT_MIDL_NM==undefined)
	{
		document.getElementById("middle_name").value =" ";
	}
	else
	{
		document.getElementById("middle_name").value =jsonResponse.CONTACT.CONTACT_MIDL_NM;
	}
	if(jsonResponse.CONTACT.CONTACT_LAST_NM==undefined)
	{
		document.getElementById("last_name").value =" ";
	}
	else
	{
		document.getElementById("last_name").value =jsonResponse.CONTACT.CONTACT_LAST_NM;
	}	
	if(jsonResponse.CONTACT.CONTACT_FULL_NM==undefined)
	{
		document.getElementById("full_name").value =" ";
	}
	else
	{
		document.getElementById("full_name").value =jsonResponse.CONTACT.CONTACT_FULL_NM;
	}
	if(jsonResponse.CONTACT.CONTACT_SFX_CD==undefined)
	{
		document.getElementById("Suffix").value =" ";
	}
	else
	{
		document.getElementById("Suffix").value =jsonResponse.CONTACT.CONTACT_SFX_CD;
	}
	if(jsonResponse.CONTACT.CONTACT_BUS_PHONE==undefined)
	{
		document.getElementById("bus_phone").value =" ";
	}
	else
	{
		document.getElementById("bus_phone").value =jsonResponse.CONTACT.CONTACT_BUS_PHONE;
	}
	if(jsonResponse.CONTACT.CONTACT_MOBILE_PHONE==undefined)
	{
		document.getElementById("mob_phone").value = " ";
	}
	else
	{
		document.getElementById("mob_phone").value =jsonResponse.CONTACT.CONTACT_MOBILE_PHONE;
	}
	if(jsonResponse.CONTACT.CONTACT_STAT_FAX_NUMBER==undefined)
	{
		document.getElementById("fax_phone").value =" ";
	}
	else
	{
		document.getElementById("fax_phone").value =jsonResponse.CONTACT.CONTACT_STAT_FAX_NUMBER;
	}
	if(jsonResponse.CONTACT.CONTACT_STAT_EMAIL==undefined)
	{
		document.getElementById("email").value =" ";
	}
	else{
		document.getElementById("email").value =jsonResponse.CONTACT.CONTACT_STAT_EMAIL;
	}
}
</script>
</head>
<body>
    <h2 style="color:#3399ff;">Search Before Create</h2><br>
    
    <select onchange="yesnoCheck()">
    <option id="">Choose Search Criteria</option>
    <option id="noCheck" value="ContactName">Name</option>
    <option id="yesCheck" value="ContactNumber">Number</option>
</select>
<br><br>
<div id="cntctName" style="display: none;" onkeyup="javascript:hinter(event)">
    <label for="name">Name</label> 
    <input type="text" id="cname" name="cname" onkeyup="javascript:hinter(event)" list="huge_list"><br/>
    <datalist id="huge_list">
    </datalist>
</div>
<div id="cntctNumber" style="display: none;">
    <label for="number">Number</label> 
    <input type="text" id="cnumber" name="cnumber" onkeyup="javascript:hinter(event)" list="huge_list"/><br />
    <datalist id="huge_list">
    </datalist>
</div>
<script type="text/javascript">
    function yesnoCheck() {
        if (document.getElementById("yesCheck").selected) {
            document.getElementById("cntctName").style.display = "none";
            document.getElementById("cntctNumber").style.display = "block";
             window.hinterXHR.abort(); 
        } 
        else {
        	document.getElementById("cntctName").style.display = "block";
            document.getElementById("cntctNumber").style.display = "none";
             window.hinterXHR.abort();
      }
    }    
</script>
    <form action="javascript:updateField()">
       <!-- Name : <input type="text" name="name" id="name_input" list="huge_list">
        <datalist id="huge_list">
        </datalist> -->
        <br/>
        <br/>
        <button class="button" type="submit" formtarget="_self">Lookup</button>
        <br><br><br><br>
        		Salutation&nbsp;&nbsp;&nbsp;&nbsp;<input  name="Salutation" id="Salutation"/><br><br>
    			First Name&nbsp;&nbsp;<input name="first_name" id="first_name"/>&nbsp;&nbsp;&nbsp;&nbsp;
    	    	Middle Name&nbsp;&nbsp;<input name="middle_name" id="middle_name"/>&nbsp;&nbsp;&nbsp;&nbsp;
    	    	Last Name&nbsp;&nbsp;<input  name="last_name" id="last_name"/>
    	    	Suffix&nbsp;&nbsp;<input  name="Suffix" id="Suffix"/><br><br>
    	    	Full Name&nbsp;&nbsp;&nbsp;<input  name="full_name" id="full_name"/><br><br>
    	    	Business Phone&nbsp;&nbsp;<input  name="bus_phone" id="bus_phone"/>&nbsp;&nbsp;&nbsp;&nbsp;
    	    	Mobile Number&nbsp;&nbsp;<input  name="mob_phone" id="mob_phone"/><br><br>
    	    	Fax &nbsp;&nbsp;<input name="fax_phone" id="fax_phone"/>&nbsp;&nbsp;&nbsp;&nbsp;
    	    	Email&nbsp;&nbsp;<input  name="email" id="email"/>  	    	
    </form>
</body>
</html>